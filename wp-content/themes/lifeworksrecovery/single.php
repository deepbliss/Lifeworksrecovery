<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

 <section class="inner-banner">
        <img src="<?php echo get_template_directory_uri(); ?>/images/inner-banner.jpg" alt="">
        <div class="breadscrumb">
        <div class="container">
            <div class="bread-block">
                <?php
                    bcn_display();                
                ?>
            </div>
        </div>
    </div>
</section>
<section class="blog">
    <div class="container">
        <div class="blog-inner-content">
            <?php 
            if ( have_posts() ) {
                while ( have_posts() ) {
                    the_post(); 
                    the_content();
                } // end while
            } // end if
            ?>
            <div class="tag">
                <?php
                    echo get_the_tag_list('<p>Tags: ',', ','</p>');
                ?>
            </div>
            <?php
                  if ( comments_open() || get_comments_number() ) :
                     comments_template();
                 endif;   
            ?>
        </div>
        <div class="right-sidebar">
            <div class="latest-post">
                <h2>LATEST POSTS</h2>
                <ul>
                    <?php
                        $args = array( 'numberposts' => '5' );
                        $recent_posts = wp_get_recent_posts( $args );
                        foreach( $recent_posts as $recent ){
                            echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
                        }
                        wp_reset_query();
                    ?>
                </ul>    
            </div>
             <div class="archives">
                <h2>ARCHIVES</h2>
                <ul>
                    <?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12 ) ); ?>
                </ul>    
            </div>
            <div class="all-tags">
                <h2>Tags</h2>
                <ul>
                    <?php 
                    $tags = get_tags();
                    $html = '<div class="post_tags">';
                    foreach ( $tags as $tag ) {
                        $tag_link = get_tag_link( $tag->term_id );

                        $html .= "<li><a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
                        $html .= "{$tag->name}</a></li>";
                    }
                    $html .= '</div>';
                    echo $html;
                 
                    ?>
                </ul>
            </div>
        </div>  
    </div>    
</section>

<?php get_footer();
