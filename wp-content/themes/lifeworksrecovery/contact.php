<?php
/*
Template Name:Contact
*/

get_header(); ?>
    <section class="inner-banner">
        <img src="<?php the_field('inner_banner');?>" alt="">
        <div class="breadscrumb">
        <div class="container">
            <div class="bread-block">
                <?php
                    bcn_display();                
                ?>
            </div>
        </div>
    </div>
    </section>
    
    <section class="contact">
        <div class="container">
            <?php 
                if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post(); 
                        the_content();
                    } // end while
                } // end if
            ?>
        </div>
    </section>
<?php get_footer();
