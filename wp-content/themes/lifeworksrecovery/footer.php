<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<?php 
    $shortnametheme = "lwr";
?>
<footer class="footer-border">	
	<div class="container">
		<div class="inner-footer">
			<div class="footer-lft">
			<div class="footer-menu">
			<ul>
				<?php wp_nav_menu( array('menu' => 'Footer menu 1',  'items_wrap' => '%3$s', 'container' => 'false', )); ?>
			</ul>
			</div>
			<div class="footer-menu">
			<ul>
				<?php wp_nav_menu( array('menu' => 'Footer menu 2',  'items_wrap' => '%3$s', 'container' => 'false', )); ?>
			</ul>
			</div>
			</div>
			<div class="footer-rgt">
				<div class="newsletter-main"><h6>Newsletter Sign Up</h6>
				<div class="sub-news">Sign up for events and recovery updates.</div>
				<div class="newsletter-sec"><?php echo do_shortcode ('[mailpoet_form id="1"]');?></div>
				</div>
				<div class="footer-social-link">
							<ul>
								<li class="facebook"><a href="<?php echo stripslashes(get_option($shortnametheme."_fblink")); ?>"></a></li>
								<li class="twitter"><a href="<?php echo stripslashes(get_option($shortnametheme."_twlink")); ?>"></a></li>
								
								<li class="gplus"><a href="<?php echo stripslashes(get_option($shortnametheme."_gblinkic")); ?>"></a></li>
								
							</ul>
				</div>
			</div>
		</div>
		
	</div>
	
	<div class="copyright">
			<p>Copyright © <?php echo date('Y');?> Life Works Recovery, All Rights Reserved.  &nbsp;  |  &nbsp; Website designed by <a href="http://iovista.com/" target="_blank">ioVista, Inc.</a></p>
	</div>
</footer>
		</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mmenu.min.all.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>


<script>
$('.home-slide').owlCarousel({
    loop:false,
    margin:10,
    items:1,
    mouseDrag:false,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
})
</script>


<script type="text/javascript">
$(function() {
$('nav#menu').mmenu({
extensions : [ 'effect-slide-menu', 'pageshadow' ],
searchfield : false,
counters : false,
navbar : {
title : 'Menu'
},
navbars : [
{
position : 'top',
<!--content : [ 'searchfield' ]-->               
}, {
position : 'top',
content : [
'prev',
'title',
'close'
]
}
]
});
});

</script>
<script type="text/javascript">
$(window).scroll(function() {
if ($(this).scrollTop() > 1){  
    $('.header-menu-sec').addClass("sticky");
  }
  else{
    $('.header-menu-sec').removeClass("sticky");
  }
});
</script>

 <script type="text/javascript">
if ($.fn.owlCarousel) {
var autoplay = 7000,
smartSpeed_c = 700,
testi_slider = $('.slider1');
//Hero_Slider_crsl

testi_slider.owlCarousel({
nav: true,
dots: false,
autoplay: true,
margin: 0,
lazyLoad: true,
loop: true,
items: 1,
animation: true,
touchDrag:true,
mouseDrag:false,
animateIn: 'fadeIn',

autoplayTimeout: autoplay,
responsiveClass:true,
         
});
}         
</script>
<script>
$('.fancybox').fancybox();
</script>
<script type="text/javascript">
$(window).scroll(function() {
if ($(this).scrollTop() > 1){  
    $('header').addClass("sticky");
  }
  else{
    $('header').removeClass("sticky");
  }
});
</script>


<?php
add_action( 'wp_footer', 'cf7_thank_you_redirect' );
 
function cf7_thank_you_redirect() {
?>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ( '89' == event.detail.contactFormId ) {
        location = '<?php echo site_url(); ?>/thank-you';
    } else if ( '517' == event.detail.contactFormId ) {
        location = '<?php echo site_url(); ?>/thank-you-1';
    } else {
        // do nothing
    }
}, false );
</script>
<?php
}       
?>


<script type="text/javascript">
jQuery('ul.mind').each(function(){
   var $ul = jQuery(this),
       $lis = $ul.find('li:gt(0)'),
       isExpanded = $ul.hasClass('expanded');
   $lis[isExpanded ? 'show' : 'hide']();
   
   if($lis.length > 0){
       $ul.append(jQuery('<span class="showmore"><p class="expand">' + (isExpanded ? '- Read Less' : '+ Read More') + '</p></span>')
           .click(function(event){
               var isExpanded = $ul.hasClass('expanded');
               event.preventDefault();
                jQuery(this).html(isExpanded ? '<p class="expand">+ Read More</p>' : '<p class="expand">- Read Less</p>');
               $ul.toggleClass('expanded');
               $lis.toggle();
           }));
   }
});

</script>
      

<?php wp_footer(); ?>

</body>
</html>
