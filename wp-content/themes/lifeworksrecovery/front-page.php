<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<section>
	<div class="slider-sec">
		<?php do_shortcode('[slider]'); ?>
	</div>
</section>

<section>
	<div class="container">
	<div class="hservice-sec">
		<div class="hservice-title">	<h2><?php the_field('homeservice_title'); ?></h2></div>
		 <div class="hser-sub-text">		<?php the_field('homeservice_sub_title'); ?></div>
		<div class="service-sec">
			<?php $args = array( 'post_type' => 'service', 'posts_per_page' => 9, 'order' => 'ASC' );
			$the_query = new WP_Query($args);
			if($the_query -> have_posts())
			{ ?>
			<ul>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<?php $fullimg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', true, '' );
			$thumimg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 200,200 ), false, '' ); ?>
			<li>
			<a href="<?php the_field('homeservice_page_link'); ?>">                          
			<div class="service-image">
			<figure><span><img src="<?php echo $fullimg[0]; ?>" data-bgfit="cover" data-bgrepeat="no-repeat" alt=""></span> </figure>

			<div class="dh-overlay"></div>
			</div>
			<div class="service-name">

			<h3><?php the_title(); ?></h3></div>

			</a>
			<div class="service-content">
			<?php the_excerpt(); ?></div>
			</li>
			<?php endwhile; ?>
			</ul>
			<?php }
			wp_reset_query(); ?>

		</div>
			<div class="hserv-info"><?php the_field('homeservice_info'); ?></div>
		
		<div class="hserv-num"><a href="tel:+19723135742"><?php the_field('homeservice_number'); ?></a></div>
	</div>
	</div>
</section>
<section class="que-bg">
	<div class="container">
		<div class="que-sec">
			<div class="question que-left">
			<div class="que-main"><?php the_field('question'); ?></div>
				<div class="que-click">
					<a href="<?php the_field('click_link_text_url'); ?>"><?php the_field('click_link_text'); ?></a>
				</div>
			</div>
			<div class="question-link">
		   <div class="question-head">	<?php the_field('center_question_text'); ?></div>
			<a href="<?php the_field('center_question_link_url'); ?>"><?php the_field('center_question_link'); ?></a>
			
			</div>
			<div class="question que-rgt">
				<div class="que-main"><?php the_field('question2'); ?></div>
				<div class="que-click">
					<a href="<?php the_field('click_link_text_url_2'); ?>"><?php the_field('click_link_text2'); ?></a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="welcome-main">
	<div class="container">
		<div class="home-welcome">
			<h4><?php the_field('home_welcome_heading'); ?></h4>
			<div class="hwelcome-sub"><?php the_field('home_welcome_sub_heading'); ?></div>
			<div class="hwelcome-text"><?php the_field('home_welcome_text'); ?></div>
			<div class="hwelcome-readmore">
				<a href="<?php the_field('home_welcome_text_url'); ?>">+ Read More</a>
			</div>
		</div>
	</div>
</section>
<section class="hproblem-main">
	<div class="container">
		<h4><?php the_field('problems_heading'); ?></h4>
		<ul>    
                  <?php if( have_rows('problems_service') ):
                       while ( have_rows('problems_service') ) : the_row(); ?>
                       
					   <li>
						<a href="<?php the_sub_field('problem_url');?>">
							
							<div class="pservice-img">
							   <figure>
								   <span>
									   <img src="<?php the_sub_field('problem_image'); ?>" alt="<?php the_sub_field('problem_title'); ?>">
								   </span>
							   </figure>
							  
						   </div>
						   <div class="pservice-rgt">
							   <h5><?php the_sub_field('problem_title'); ?></h5>
							   <div class="pservice-txt"><?php the_sub_field('problem_description'); ?></div>
						   </div>
                         </a>
                       </li>
					  
                      <?php endwhile;
                  endif; ?>
            </ul>
	</div>
</section>

<section>
	<div class="container">
		<div class="home-welcome">
			<h4><?php the_field('home_approach_heading'); ?></h4>
			<div class="hwelcome-sub"><?php the_field('home_approach_sub_heading'); ?></div>
			<div class="hwelcome-text"><?php the_field('home_approach_text'); ?></div>
			
		</div>
	</div>
</section>

<section class="que-bg">
	<div class="container">
		<div class="home-bottom-page">
			<h4><?php the_field('home_page_heading'); ?></h4>
			<div class="home-bottom-sub"><?php the_field('home_page_sub_text'); ?></div>
			<div class="hbottom-page-text"><?php the_field('home_page_content'); ?></div>
		</div>
	</div>
</section>	

<section >
	<div class="container">
		<div class="home-conatct-form">
			<h5>Contact Us Today!!</h5>
			<div class="home-conatct-inner"><?php the_field('home_contact'); ?></div>
	    </div>
	</div>
</section>

<?php get_footer();
