<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
    <section class="inner-banner">
        <img src="<?php the_field('inner_banner');?>" alt="">
        <div class="breadscrumb">
        <div class="container">
            <div class="bread-block">
                <?php
                    bcn_display();                
                ?>
            </div>
        </div>
    </div>
    </section>
    
    <section class="inner-block">
        <div class="container">
            <div class="cms-content">
            <?php 
                if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post(); 
                        the_content();
                    } // end while
                } // end if
            ?>
            </div>
            <div class="right-sidebar">
                <div class="testimonials">
                    <h2>testimonials</h2>
                    <?php echo do_shortcode('[gs_testimonial theme=”gs_style1″]');?>    
                </div>
                <div class="contact-form">
                    <h2>Contact Us Today!!</h2>
                    <?php echo do_shortcode('[contact-form-7 id="89" title="Home Contact form"]');?>
                </div>    
            </div> 
        </div>
    </section>
<?php get_footer();
