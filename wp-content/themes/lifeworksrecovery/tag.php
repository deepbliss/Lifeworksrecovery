<?php
/**
 * The template for displaying Tag pages
 *
 * Used to display archive-type pages for posts in a tag.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
get_header(); ?>

    <section class="inner-banner">
        <img src="<?php echo get_template_directory_uri(); ?>/images/inner-banner.jpg" alt="">
        <div class="breadscrumb">
        <div class="container">
            <div class="bread-block">
                <?php
                    bcn_display();                
                ?>
            </div>
        </div>
    </div>
</section>
<section class="blog">
    <div class="container">
        <div class="blog-content">
           <ul>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <li>
                   <div class="post_info">
                        <span class="date"><a href="<?php the_permalink(); ?>"><?php echo get_the_date( 'Y-m-d' ); ?></a></span> | <span class="author">by <b><?php echo get_the_author();?></b></span>
                   </div>
                   <div class="title">
                        <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
                   </div>
                   <div class="excerpt">
                        <p><?php echo wp_trim_words( get_the_excerpt(), 30, '...' ); ?></p>
                   </div>
                   <a class="readmore" href="<?php the_permalink(); ?>">Read More</a>
                </li>
                <?php endwhile;?>
                
                <?php 
                                if(function_exists('wp_paginate')):
                    wp_paginate();  
                else :
                    the_posts_pagination( array(
                        'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
                        'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
                        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
                    ) );
                endif;
                ?>
               <?php   else : ?>
                
                <?php endif; ?>
           </ul>     
        </div>
        <div class="right-sidebar">
            <div class="latest-post">
                <h2>LATEST POSTS</h2>
                <ul>
                    <?php
                        $args = array( 'numberposts' => '5' );
                        $recent_posts = wp_get_recent_posts( $args );
                        foreach( $recent_posts as $recent ){
                            echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
                        }
                        wp_reset_query();
                    ?>
                </ul>    
            </div>
             <div class="archives">
                <h2>ARCHIVES</h2>
                <ul>
                    <?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12 ) ); ?>
                </ul>    
            </div>
            <div class="all-tags">
                <h2>Tags</h2>
                <ul>
                    <?php 
                    $tags = get_tags();
                    $html = '<div class="post_tags">';
                    foreach ( $tags as $tag ) {
                        $tag_link = get_tag_link( $tag->term_id );

                        $html .= "<li><a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
                        $html .= "{$tag->name}</a></li>";
                    }
                    $html .= '</div>';
                    echo $html;
                 
                    ?>
                </ul>
            </div>
        </div>  
    </div>    
</section>
<?php get_footer(); ?>