<?php
/**
 * Template Name: Contact Page
 *
 **/
get_header(); ?>



 <section class="banner" style='background: #f5f7f6 url("<?php $bgimg = get_field("inner_banner_image"); if($bgimg != "") { the_field("inner_banner_image"); } else { ?><?php echo esc_url(    get_template_directory_uri() ); ?>/images/banner2.png<?php } ?>") no-repeat center center;'>

		 <div class="slider-text">
		<div class="container">
			<?php echo the_content(); ?>
			<h1><?php the_field( 'inner_page_text' ); ?></h1>
			<p><?php the_field( 'inner_page_sub_text' ); ?></p>
			
         </div>
		</div> 
		<div class="banner-rgt-img"><img src="<?php the_field( 'inner_page_right_side_image' ); ?>"></div>
   </section>
   
    <section class="inner-sec contact-page">
        <div class="container">
		<h2><?php  the_title();  ?></h2>
			<div class="contact-inner">
				<div class="contact-lft">
					
							 <?php
						while ( have_posts() ) : the_post();
						the_content();
						endwhile; 
						?>
				</div>
				<div class="contact-rgt">
					<div class="add-lft">
						<div class="address-sec">
							<h3>ADDRESS</h3>
								<div class="address-inner"><?php the_field( 'contact_address' ); ?>	</div>	
						</div>
						<div class="phonenumber-sec">
							<h3>PHONE</h3>
							
							<div class="phone-inner"><?php the_field( 'contact_phone' ); ?>	</div>	
							
						</div>
						<div class="hoursopr-sec">
							<h3>HOURS OF OPERATION</h3>
							
							<div class="hours-inner"><p><?php the_field( 'contact_hours' ); ?>	</p></div>	
						</div>
					</div>
					<div class="team-rgt">
						<h3>TEAM CONTACT</h3>
						<div class="team-number">
								<ul>    
								  <?php if( have_rows('contact_team') ):
									   while ( have_rows('contact_team') ) : the_row(); ?>
									   <li>
										<a href="mailto:<?php the_sub_field('person_url'); ?>">
											<div class="person-name"><?php the_sub_field('person_name'); ?> ~ </div>

											<div class="person-number">
											&nbsp;<?php the_sub_field('person_extension'); ?></div>
										</a> 
									   </li>
									  <?php endwhile;
								  endif; ?>
								</ul>
						</div>
					</div>	
				</div>			
			</div>			
		</div>
		
	</section>
	 <section>
		<div class="contact-map">
			<?php the_field( 'contact_map' ); ?>
		</div>
	</section>	
<?php get_footer(); ?>