<?php
/**
 * Template Name: About Page
 *
 **/
get_header(); ?>



 <section class="banner" style='background: #f5f7f6 url("<?php $bgimg = get_field("inner_banner_image"); if($bgimg != "") { the_field("inner_banner_image"); } else { ?><?php echo esc_url(    get_template_directory_uri() ); ?>/images/banner2.png<?php } ?>") no-repeat center center;'>

		 <div class="slider-text">
		<div class="container">
			<?php echo the_content(); ?>
			<h1><?php the_field( 'inner_page_text' ); ?></h1>
			<p><?php the_field( 'inner_page_sub_text' ); ?></p>
			
         </div>
		</div> 
		<div class="banner-rgt-img"><img src="<?php the_field( 'inner_page_right_side_image' ); ?>"></div>
   </section>
   
    <section class="inner-sec">
        <div class="container">
          <div class="inner-lft">	
               <h2><?php  the_title();  ?></h2>
							
						 <?php
					while ( have_posts() ) : the_post();
					the_content();
					endwhile; 
					?>
				<div class="our-mission">
					<div class="our-mission-lft">
						<img src="<?php the_field( 'our_mission_image' ); ?>">
					</div>
					<div class="our-mission-rgt">
					<h3><?php the_field( 'our_mission_text' ); ?></h3>
					<?php the_field( 'our_mission_description' ); ?>
					</div>					
				</div>
			</div>
				<div class="inner-rgt">	
					<div class="about-rgt-top">
						<h3><?php the_field( 'about_right_text' ); ?></h3>
						<div class="argt-timg">
						<img src="<?php the_field( 'about_right_image' ); ?>">
						</div>	
					</div>
					<div class="about-rgt-btm">
						<div class="hcustomer-part">
				<h4>OUR CUSTOMERS</h4>
				<?php $args = array( 'post_type' => 'customers', 'posts_per_page' => 100, 'order' => 'ASC' );
$the_query = new WP_Query($args);
if($the_query -> have_posts())
{ ?>
<ul class="side-client owl-carousel">
<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

 <li>
							
							<a href="<?php the_field('customers_logo_url'); ?>">
								<figure>
									   <span>
										   <img src="<?php the_field('customers_logo_image'); ?>" alt="<?php the_title ?>" title="<?php the_title ?>">
									   </span>
								   </figure>
								  <h5><?php the_title(); ?></h5>
							</a> 
							
						</li>
<?php endwhile; ?>
</ul>
<?php }
wp_reset_query(); ?>
								 
							
				
			</div>
			
			<div class="hcustomer-part">
			<h4>ASSOCIATIONS</h4>
			<?php $args = array( 'post_type' => 'associations', 'posts_per_page' => 100, 'order' => 'ASC' );
$the_query = new WP_Query($args);
if($the_query -> have_posts())
{ ?>
<ul class="side-ass owl-carousel">
<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

 <li>
							
							<a href="<?php the_field('associations_url'); ?>">
								<figure>
									   <span>
										   <img src="<?php the_field('associations_image'); ?>" alt="<?php the_title ?>" title="<?php the_title ?>">
									   </span>
								   </figure>
								  <h5><?php the_title(); ?></h5>
							</a> 
							
						</li>
<?php endwhile; ?>
</ul>
<?php }
wp_reset_query(); ?>
			</div>
			
			
					</div>
					
				</div>				
		</div>
		
	 </section>
<?php get_footer(); ?>