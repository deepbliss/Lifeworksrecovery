<?php
/**
 * Template Name: Current Bid list Page
 *
 **/
get_header(); ?>



 <section class="banner" style='background: #f5f7f6 url("<?php $bgimg = get_field("inner_banner_image"); if($bgimg != "") { the_field("inner_banner_image"); } else { ?><?php echo esc_url(    get_template_directory_uri() ); ?>/images/banner2.png<?php } ?>") no-repeat center center;'>

		 <div class="slider-text">
		<div class="container">
			<?php echo the_content(); ?>
			<h1><?php the_field( 'inner_page_text' ); ?></h1>
			<p><?php the_field( 'inner_page_sub_text' ); ?></p>
			
         </div>
		</div> 
		<div class="banner-rgt-img"><img src="<?php the_field( 'inner_page_right_side_image' ); ?>"></div>
   </section>
   
    <section class="inner-sec current-page">
        <div class="container">
          <h2><?php  the_title();  ?></h2>
			<?php
					while ( have_posts() ) : the_post();
					the_content();
					endwhile; 
					?>
			<div class="current-sec" id="no-more-tables">
				<table width="100%" align="center" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							
							<th>Project Data & Plan Holder List</th>
							<th>Project Name</th>
							<th>Download Bid Results</th>
							<th>Download Plans & Contract Documents(password protected)</th>
							<th>Project Addendum's</th>
						</tr>
						</thead>
					<tbody>	
						<tr>
						 <?php if( have_rows('current_bid_list') ):
									   while ( have_rows('current_bid_list') ) : the_row(); ?>
							<td data-th="Project Data & Plan Holder List"><?php the_sub_field('project_data'); ?></td>
							<td data-th="Project Name"><?php the_sub_field('project_name'); ?></td>
							<td data-th="Download Bid Results"><?php the_sub_field('download_bid'); ?></td>
							<td data-th="Download Plans & Contract Documents(password protected)"> <?php the_sub_field('download_plans'); ?></td>
							<td data-th="Project Addendum's"><?php the_sub_field('project_addendums'); ?></td>
							
						</tr>
						 <?php endwhile;
								  endif; ?>
						
					</tbody>
				</table>
			</div>
		
			<p>Showing 1 to 17 of 17 entries</p>
		</div>
		
	 </section>
<?php get_footer(); ?>