<?php
/**
 * Template Name: Service Page
 *
 **/
get_header(); ?>

 <section class="banner" style='background: #f5f7f6 url("<?php $bgimg = get_field("inner_banner_image"); if($bgimg != "") { the_field("inner_banner_image"); } else { ?><?php echo esc_url(    get_template_directory_uri() ); ?>/images/banner2.jpg<?php } ?>") no-repeat center center;'>

		 <div class="slider-text">
		<div class="container">
			<?php echo the_content(); ?>
			<h1><?php the_field( 'inner_page_text' ); ?></h1>
			<p><?php the_field( 'inner_page_sub_text' ); ?></p>
			
         </div>
		</div> 
		<div class="banner-rgt-img"><img src="<?php the_field( 'inner_page_right_side_image' ); ?>"></div>
   </section>

    <section class="inner-sec">
        <div class="container">
          
               <h2><?php  the_title();  ?></h2>
								
				
				
				
							
						<div class="service-sec">
										<?php $args = array( 'post_type' => 'service', 'posts_per_page' => 100, 'order' => 'ASC' );
$the_query = new WP_Query($args);
if($the_query -> have_posts())
{ ?>
<ul>
<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
<?php $fullimg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', true, '' );
$thumimg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 200,200 ), false, '' ); ?>
<li>
<a href="<?php the_permalink(); ?>">                          
<div class="service-image">
 <figure><span><img src="<?php echo $fullimg[0]; ?>" data-bgfit="cover" data-bgrepeat="no-repeat" alt=""></span> </figure>

 <div class="dh-overlay"></div>
</div>
<div class="service-name">

<h2><?php the_title(); ?></h2></div>

</a>
<div class="service-content">
 <?php the_excerpt(); ?></div>
</li>
<?php endwhile; ?>
</ul>
<?php }
wp_reset_query(); ?>
								 
						</div>
				<div class="project-line">
					<div class="inner-pro-line">
						<p><?php the_field( 'project_text' ); ?></p>
					</div>
				</div>
	
        </div>
    </section>
	
	



<?php get_footer(); ?>