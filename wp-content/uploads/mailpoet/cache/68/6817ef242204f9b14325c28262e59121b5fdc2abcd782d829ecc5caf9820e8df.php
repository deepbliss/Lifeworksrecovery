<?php

/* form/templates/blocks/divider.hbs */
class __TwigTemplate_e18f96a3f447425a5ecad7a7ccb4868f8d41981642ed7e01fc104a968bd348b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<hr />";
    }

    public function getTemplateName()
    {
        return "form/templates/blocks/divider.hbs";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "form/templates/blocks/divider.hbs", "D:\\xampp\\htdocs\\lifeworksrecovery\\wp-content\\plugins\\mailpoet\\views\\form\\templates\\blocks\\divider.hbs");
    }
}
