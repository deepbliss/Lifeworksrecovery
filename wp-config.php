<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'iovista_lifeworksrecovery');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'ioVist@r0cks');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>Ksun4R@?XA`~ek+,Ku1C9_1R0RgrJ-_#*nj53q?~ex(4(0!jMb{6Rl`h C*!Wv,');
define('SECURE_AUTH_KEY',  'pvueHC5qDaCtaXsHC>z#V`]m DlCPLe=eYZ%ZooZb&x_Vlbb~3^aRI+/smkjSu0Z');
define('LOGGED_IN_KEY',    '(su3et<Ky6ApH8;h9#{_S5:c<d6n#aXDlB.oW2XKqWGwGtT%/u/gH!ZtGm8MumAj');
define('NONCE_KEY',        '>u;ne}Mj/xVc)+El)i#JTu!Leghi]8]X/]-qr(q_IzvW)|Ois U0fx7ZD7K!slP;');
define('AUTH_SALT',        'pMZ+rGm}hqtg5 Poe,9#C9**J9Ug%P!<f*c(YzR{cGwwR@+XIoIxhilPBMt130Ou');
define('SECURE_AUTH_SALT', '5ZRjE?.zZ1QVxpiNgHOT3ehkp1=;l|Kq7d-z/]Hf`>kD` @YMD]broQ/?pr&G!Tb');
define('LOGGED_IN_SALT',   'YV}D`;{y5VrNbtPaKG~lBBhR[uG4RgUc[NJYv{UKgU#Xl[^ZJDW-gU<~B{}%}=o(');
define('NONCE_SALT',       'lMv7i{7u02Nmzb*Z#ca<px+C4b]Sw+|4517t/+lrS3X~>%P`2]4|4t J@pv4)Dr;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
